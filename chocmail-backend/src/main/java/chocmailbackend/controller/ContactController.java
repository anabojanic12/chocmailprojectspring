package chocmailbackend.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import chocmailbackend.dto.ContactDTO;
import chocmailbackend.entity.Contact;
import chocmailbackend.entity.Photo;
import chocmailbackend.entity.User;
import chocmailbackend.service.ContactService;
import chocmailbackend.service.PhotoService;
import chocmailbackend.service.UserService;

@RestController
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	private ContactService contactService;

	@Autowired
	private UserService userService;
	
	
	@Autowired
	private PhotoService photoService;
	
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<List<ContactDTO>> getAllContacts(Principal principal) {

		User user = userService.findByUsername(principal.getName());

		List<ContactDTO> contacts = new ArrayList<>();

		for (Contact contact : contactService.findAllContacts(user)) {
			contacts.add(new ContactDTO(contact));
		}
		
		return new ResponseEntity<List<ContactDTO>>(contacts, HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("/{id}")
	public ResponseEntity<?> getContactById(
			@PathVariable("id") Integer contactId,
			Principal principal)
	{
		User user = userService.findByUsername(principal.getName());

		Contact contact = contactService.findOne(contactId, user);
		
		if (contact == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
			
	
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
	}
	
	@CrossOrigin
	@PostMapping
	public ResponseEntity<?> saveContact(@RequestBody ContactDTO contact,Principal principal) {

		User user = userService.findByUsername(principal.getName());
		
		Contact nContact = new Contact();
		nContact.setFirstName(contact.getFirstName());
		nContact.setLastName(contact.getLastName());
		nContact.setDisplayName(contact.getDisplayName());
		nContact.setEmail(contact.getEmail());
		nContact.setNote(contact.getNote());
		nContact.setPhotos(new ArrayList<>());
		nContact.setUser(user);
		nContact = contactService.save(nContact);
		return new ResponseEntity<>(HttpStatus.CREATED);


		
	}
	
	@CrossOrigin
	@PutMapping
	public ResponseEntity<?> updateContact(@RequestBody ContactDTO contact,Principal principal) {

		User user = userService.findByUsername(principal.getName());

		Contact econtact = contactService.findOne(contact.getId(), user);

		if(econtact!=null) {
			econtact.setDisplayName(contact.getDisplayName());
			econtact.setFirstName(contact.getFirstName());
			econtact.setLastName(contact.getLastName());
			econtact.setNote(contact.getNote());
			econtact.setEmail(contact.getEmail());
			econtact.setUser(user);
			econtact = contactService.save(econtact);
			return new ResponseEntity<>(HttpStatus.OK);
		}else {
			return new ResponseEntity<>("You are unauthorized to do that.", HttpStatus.UNAUTHORIZED);
		}

	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteContact(
			@PathVariable("id") Integer contactId,
			Principal principal)
	{
		User user = userService.findByUsername(principal.getName());

		Contact contact = contactService.findOne(contactId, user);

		if (contact != null) {
			contactService.remove(contactId);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

}
