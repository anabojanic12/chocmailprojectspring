package chocmailbackend.controller;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import chocmailbackend.dto.UserDTO;
import chocmailbackend.entity.Account;
import chocmailbackend.entity.Contact;
import chocmailbackend.entity.Tag;
import chocmailbackend.entity.User;
import chocmailbackend.security.TokenHelper;
import chocmailbackend.security.model.AuthenticationRegister;
import chocmailbackend.security.model.AuthenticationRequest;
import chocmailbackend.security.model.UserAuthenticationToken;
import chocmailbackend.service.UserService;

@RestController
@RequestMapping("/register")
public class RegisterContoller {

	
	//@Autowired
	//private AuthenticationManager authenticationManager;
	
	//@Autowired
	//private TokenHelper tokenHelper;
	
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping
	public ResponseEntity<?> authenticateUser(@RequestBody AuthenticationRegister userDTO) /*throws AuthenticationException, IOException */ {
		
	/*	UsernamePasswordAuthenticationToken authenticationRequest = new UsernamePasswordAuthenticationToken(tokenRequest.getUsername(),tokenRequest.getPassword());
		
		final Authentication authentication;
		
		try {
			authentication = authenticationManager.authenticate(authenticationRequest);
		} catch (AuthenticationException ex) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		
		SecurityContextHolder.getContext().setAuthentication(authentication);*/
		
		
				if(userDTO.getFirstName() == null) {
					return new ResponseEntity<String>("First name can't be empty",HttpStatus.BAD_REQUEST);
				}else if(userDTO.getLastName() == null) {
					return new ResponseEntity<String>("Last name can't be empty",HttpStatus.BAD_REQUEST);
				}else if(userDTO.getUsername() == null) {
					return new ResponseEntity<String>("Username can't be empty",HttpStatus.BAD_REQUEST);
				}else if(userDTO.getPassword() == null) {
					return new ResponseEntity<String>("Password can't be empty",HttpStatus.BAD_REQUEST);
				}
				
				// -- Check check that username already exists
				User usernameTaken = userService.findByUsername(userDTO.getUsername());
				if (usernameTaken != null) {
					return new ResponseEntity<String>("Username already exists",HttpStatus.BAD_REQUEST);
				}
				
				
				User user = new User();
				
				user.setFirstName(userDTO.getFirstName());
				user.setLastName(userDTO.getLastName());
				user.setUsername(userDTO.getUsername());
				user.setPassword(userDTO.getPassword());
				user.setAccounts(new ArrayList<Account>());
				user.setContacts(new ArrayList<Contact>());
				user.setTags(new ArrayList<Tag>());
				
				// ----- Saving new user to database -----
				user = userService.save(user);
				
				//s user = (User) authentication.getPrincipal();
				 System.out.println("sta je u ovom slucaju user : "+ user.getUsername());
				
			//	String token = tokenHelper.generateToken(user.getUsername());
			//	long expiresIn = tokenHelper.getExiprationDate(token);
				
				//return ResponseEntity.ok(new UserAuthenticationToken(token, expiresIn));
		
				return new ResponseEntity<String>("Success",HttpStatus.OK);
	
		
		
	}
}
