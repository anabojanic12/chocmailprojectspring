package chocmailbackend.common;

import java.util.List;

import chocmailbackend.entity.Folder;
import chocmailbackend.entity.Message;

public class MoveRuleHandler implements RuleHandler {

	/**
	 * Moves an e-mail to the passed folder.
	 * 
	 * @param List containing all messages.
	 * @param Message index in that list.
	 * @param Folder where you want to move the e-mail.
	 */
	@Override
	public void applyRule(List<Message> messages, int index, Folder folder) {
		Message message = messages.get(index);
		
		message.setFolder(folder);
		folder.getMessages().add(message);
	}
	
}
