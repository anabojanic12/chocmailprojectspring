package chocmailbackend.common;

import java.util.List;

import chocmailbackend.entity.Folder;
import chocmailbackend.entity.Message;

/**
 * Has a method that applies a certain rule on a folder.
 */
public interface RuleHandler {

	public void applyRule(List<Message> messages, int index, Folder folder);
	
}
