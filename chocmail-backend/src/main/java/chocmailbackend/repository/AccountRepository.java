package chocmailbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.Account;
import chocmailbackend.entity.User;

public interface AccountRepository extends JpaRepository<Account, Integer> {

	public List<Account> findByUser(User user);
	
	public Account findByIdAndUser(Integer id, User user);
	
}
