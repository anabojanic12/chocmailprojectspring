package chocmailbackend.repository;

import java.awt.print.Pageable;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import chocmailbackend.entity.Account;
import chocmailbackend.entity.Folder;
import chocmailbackend.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Integer>{

	public Message findByIdAndAccount(Integer id, Account account);
	
	public List<Message> findAllByUnreadIsTrue();
	
	public List<Message> findAllByAccount(Account account);
	
	public List<Message> findAllByAccountAndFolder(Account account, Folder folder);
	
	@Query(value = "SELECT MAX(date_time) FROM messages WHERE account = :accountId", nativeQuery = true)
	public Date findLastDate(@Param("accountId") int accountId);
	
	public List<Message> findByFromContaining(String userEmail);
	}
