package chocmailbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.Attachment;

public interface AttachmentRepository extends JpaRepository<Attachment, Integer>{

}
