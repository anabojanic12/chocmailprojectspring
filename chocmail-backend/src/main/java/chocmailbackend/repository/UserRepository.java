package chocmailbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUsername(String username);
	
}
