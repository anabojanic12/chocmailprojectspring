package chocmailbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.Folder;
import chocmailbackend.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Integer> {

	public Rule findByIdAndFolder(Integer id, Folder folder);
	
	public List<Rule> findAllByFolder(Folder folder);
	
}
