package chocmailbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.Contact;
import chocmailbackend.entity.User;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
	
	public List<Contact> findAllByUser(User user);
	
	public Contact findByIdAndUser(Integer id,User user);
	


}
