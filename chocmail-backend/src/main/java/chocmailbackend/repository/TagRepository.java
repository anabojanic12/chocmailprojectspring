package chocmailbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import chocmailbackend.entity.Tag;
import chocmailbackend.entity.User;

public interface TagRepository extends JpaRepository<Tag, Integer>{

	public Tag findByIdAndUser(Integer id, User user);
	
}
