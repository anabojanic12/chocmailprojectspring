package chocmailbackend.dto;

import chocmailbackend.entity.Rule;
import chocmailbackend.entity.Rule.RuleCondition;
import chocmailbackend.entity.Rule.RuleOperation;

public class RuleDTO {
	// ----- FIELDS
	private Integer id;
	private RuleCondition condition;
	private String value;
	private RuleOperation operation;
	
	public RuleDTO(Rule rule) {
		super();
		this.id = rule.getId();
		this.condition = rule.getCondition();
		this.value = rule.getValue();
		this.operation = rule.getOperation();
	}

	public RuleDTO() {
		
	}
	
	// ----- GETTERS AND SETTERS
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public RuleCondition getCondition() {
		return condition;
	}

	public void setCondition(RuleCondition condition) {
		this.condition = condition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public RuleOperation getOperation() {
		return operation;
	}

	public void setOperation(RuleOperation operation) {
		this.operation = operation;
	}
	
}
