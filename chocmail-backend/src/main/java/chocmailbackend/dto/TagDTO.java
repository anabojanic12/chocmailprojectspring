package chocmailbackend.dto;

import java.util.ArrayList;
import java.util.List;

import chocmailbackend.entity.Message;
import chocmailbackend.entity.Tag;

public class TagDTO {

	private Integer id;
	private String name;
	private List<MessageDTO> messages = new ArrayList<MessageDTO>();
	private UserDTO user;
	
	public TagDTO() {
		
	}

	public TagDTO(Tag tag) {
		super();
		this.id = tag.getId();
		this.name = tag.getName();
		// Ja bih cak i user referencu izbacio
		//this.user = new UserDTO(tag.getUser());

	/*	for (Message message : tag.getMessages()) {
			messages.add(new MessageDTO(message));
		}*/
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/*public List<MessageDTO> getMessages() {
		return messages;
	}

	public void setMessages(List<MessageDTO> messages) {
		this.messages = messages;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}*/

}
