package chocmailbackend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import chocmailbackend.entity.Tag;
import chocmailbackend.entity.User;
import chocmailbackend.repository.TagRepository;

@Service
public class TagService implements TagServiceInterface{
	
	@Autowired
	private TagRepository tagRepository;
	
	@Override
	public Tag findOne(Integer id, User user) {
		return tagRepository.findByIdAndUser(id, user);
	}
	
	@Override
	public List<Tag> findAll(){
		return tagRepository.findAll();
	}
	
	@Override
	public Tag save(Tag tag) {
		return tagRepository.save(tag);
	}

	@Override
	public void remove(Integer id) {
		tagRepository.deleteById(id);
	}
}
