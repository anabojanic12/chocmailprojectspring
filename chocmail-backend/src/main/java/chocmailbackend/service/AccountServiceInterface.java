package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Account;
import chocmailbackend.entity.User;

public interface AccountServiceInterface {
	
	public Account findOne(Integer id);
	
	public List<Account> findByUser(User user);
	
	public Account findByIdAndUser(Integer id, User user);
	
	public List<Account> findAll();
	
	public Account save(Account account);
	
	public void remove(Integer id);
	
}
