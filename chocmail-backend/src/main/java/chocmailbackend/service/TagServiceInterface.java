package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Tag;
import chocmailbackend.entity.User;

public interface TagServiceInterface {

	public Tag findOne(Integer id, User user);
	
	public List<Tag> findAll();
	
	public Tag save(Tag tag);
	
	public void remove(Integer id);
}
