package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Folder;
import chocmailbackend.entity.Rule;

public interface RuleServiceInterface {

	public Rule findOne(Integer ruleId);

	public Rule findByIdAndFolder(Integer id, Folder folder);
	
	public List<Rule> findByFolder(Folder folder);

	public List<Rule> findAll();

	public Rule save(Rule rule);

	public void remove(Integer id);

}
