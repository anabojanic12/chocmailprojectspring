package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Account;
import chocmailbackend.entity.Folder;

public interface FolderServiceInterface {
	
	public Folder findOne(Integer id, Account account);
	
	public Folder findDraft(Account account);
	
	public Folder findInbox(Account account);
	
	public Folder findOutbox(Account account);
	
	public List<Folder> findAll(Account account);
	
	public Folder save(Folder folder);
	
	public void remove(Integer id);

}
