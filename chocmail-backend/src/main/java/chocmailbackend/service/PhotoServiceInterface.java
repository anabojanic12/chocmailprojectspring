package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Contact;
import chocmailbackend.entity.Photo;

public interface PhotoServiceInterface {

	public List<Photo> findAll();
	
	public List<Photo> getPhotos(Contact contact);
	
	
	public Photo findOne(Integer photoId);
	

}
