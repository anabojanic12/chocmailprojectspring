package chocmailbackend.service;

import java.util.List;

import chocmailbackend.entity.Contact;
import chocmailbackend.entity.User;

public interface ContactServiceInterface {

	public List<Contact> findAllContacts(User user);

	public Contact findOne(Integer contactId, User user);

	public Contact save(Contact contact);

	public void remove(Integer id);

}
