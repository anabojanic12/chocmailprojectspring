INSERT INTO users (first_name, last_name, username, password) VALUES ("Chocmail", "User", "nemanja", "$2a$10$vCAPRMFq/qkgd1b7F4QGsuN249Wale.rgLG8SjDiANGXDOSEQFGYq");
INSERT INTO users (first_name, last_name, username, password) VALUES ("Ana","Bojanic","ana","$2a$10$Ddz.fXUR776rN5KAWwqQKO7ReNdfcjbGr7eFaozXxBjd7I.68J68S");

INSERT INTO accounts (smtp_address, smtp_port, in_server_type, in_server_address, in_server_port, username, password, display_name, user) VALUES ("smtp.gmail.com", 465, 1, "imap.gmail.com", 993, "chocmailuser1", "userpass1", "ChocMail Account 1", 1);
INSERT INTO accounts (smtp_address, smtp_port, in_server_type, in_server_address, in_server_port, username, password, display_name, user) VALUES ("smtp.gmail.com", 465, 1, "imap.gmail.com", 993, "chocmailuser2", "userpass2", "ChocMail Account 2", 2);

INSERT INTO folders (name, parent_folder, account) VALUES ("Inbox", null, 1);
INSERT INTO folders (name, parent_folder, account) VALUES ("Outbox", null, 1);
INSERT INTO folders (name, parent_folder, account) VALUES ("Drafts", null, 1);

INSERT INTO folders (name, parent_folder, account) VALUES ("Primary", 1, 1);
INSERT INTO folders (name, parent_folder, account) VALUES ("Spam", 1, 1);

INSERT INTO rules (rule_condition, rule_operation, value, folder) VALUES (1, 0, "nemanjamodic@gmail.com", 4);
INSERT INTO rules (rule_condition, rule_operation, value, folder) VALUES (1, 2, "chocmailuser3", 1);
INSERT INTO rules (rule_condition, rule_operation, value, folder) VALUES (1, 1, "nemzax@gmail", 4);
INSERT INTO rules (rule_condition, rule_operation, value, folder) VALUES (1, 0, "chocmailuser3", 5);

INSERT INTO folders (name, parent_folder, account) VALUES ("Inbox", null, 2);
INSERT INTO folders (name, parent_folder, account) VALUES ("Outbox", null, 2);
INSERT INTO folders (name, parent_folder, account) VALUES ("Drafts", null, 2);

INSERT INTO contacts (display_name, email, first_name, last_name, note, user) VALUES ("Nemanja", "nemanjamodic@gmail.com", "Nemanja", "Modic", "Sta god da je ovo", 1);
INSERT INTO contacts (display_name, email, first_name, last_name, note, user) VALUES ("Ana", "anabojanic.ab@gmail.com", "Ana", "Bojanic", "Sta god da je ovo", 1);
INSERT INTO contacts (display_name, email, first_name, last_name, note, user) VALUES ("Iva", "ivastojanovic@gmail.com", "Iva", "Stojanovic", "Sta god da je ovo", 1);

INSERT INTO photos (path, contact) VALUES ("images10.jpg", 1);
INSERT INTO photos (path, contact) VALUES ("image3.jpg", 2);
INSERT INTO photos (path, contact) VALUES ("image4.jpg", 3);

INSERT INTO authority (name) VALUES ("USER");

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (2, 1);

INSERT INTO tags (name, user) VALUES ("tag1", 1);

INSERT INTO messages (message_from, message_to, bcc, cc, subject, content, account, unread, folder) VALUES ("nemanjamodic@gmail.com", "chocmailuser1@gmail.com, nijeUContacts@gmail.com", "nemanjamodic@gmail.com", "nijeUContacts@gmail.com", "Naslov poruke 1", "Sadrzaj poruke 1", 1, true, 1);
INSERT INTO messages (message_from, message_to, bcc, cc, subject, content, account, unread, folder) VALUES ("iva@gmail.com", "chocmailuser1@gmail.com", "iva@gmail.com", "nemanjamodic@gmail.com", "Subject 2", "Ovo je sadrzaj neke poruke.", 2, true, 6);
INSERT INTO messages (message_from, message_to, bcc, cc, subject, content, account, unread, folder) VALUES ("winwin@shop.rs", "chocmailuser2@gmail.com", NULL, NULL, "Izvestaj isporuke", "Novi brutalan komp vam je isporucen!", 1, false, 1);

--INSERT INTO attachments (data, mime_type, name, message) VALUES ("data", "mime type", "name", 1);

--INSERT INTO message_tags (message_id, tag_id) VALUES (1,1);