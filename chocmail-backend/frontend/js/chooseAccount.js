// API url
var URL = "http://localhost:8080/acc";

$(document).ready(function() {

	var container = $("#container");

	container.on("click", ".account", function() {
		localStorage.setItem("ChocMailAccount", $(this).data("index"));

		window.location.assign("messages.html");
	});

	$.ajax({
		url: URL,
		headers: {"Authorization": "Bearer " + localStorage.getItem("ChocMailToken")},
		type: "GET",
		success: function(accounts) {
			var counter = 0;
			for (account of accounts) {
				container.append("<button class='account' data-index='" + counter + "'>" + account.displayName + "</button>");
				counter++;
			}
		}
	});

});