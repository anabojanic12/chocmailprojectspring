var URL = "http://localhost:8080/contacts";

var firstName;
var lastName;
var displayName;
var email;
var note;
var delete_contact;
var confirmButton;
var CONTACT;



$(document).ready(function() {
	
	firstName = $("input[name=firstNameInput]");
	lastName = $("input[name=lastNameInput]");
	displayName = $("input[name=displayNameInput]");
	email = $("input[name=emailInput]");
	note = $("input[name=noteInput]");
	confirmButton = $("#confirm_button_contact");
	delete_contact =  $("#delete_contact");
	
	var contactId = window.location.href.split("=")[1];
	
	
	
	if(contactId == null) {
		createContact();
	}else {
		editContact(contactId);
	}
	
	
	
	
});


function editContact(contactId) {
	
	confirmButton.text("Save changes");
	
	$.ajax({
		url: URL + "/" + contactId,
		type: "GET",
		headers: {"Authorization": "Bearer " + localStorage.getItem("ChocMailToken")},
		success: function(contact) {
			
			CONTACT = contact;
			firstName.val(CONTACT.firstName);
			lastName.val(CONTACT.lastName);
			displayName.val(CONTACT.displayName);
			email.val(CONTACT.email);
			note.val(CONTACT.note);
			
			
			confirmButton.click(function() {

				
				
				
				var data = {
					id: CONTACT.id,
					firstName : firstName.val(),
					lastName : lastName.val(),
					displayName: displayName.val(),
					email : email.val(),
					note : note.val()
				}
				
				

				$.ajax({
					url: URL,
					type: "PUT",
					headers: {"Authorization": "Bearer " + localStorage.getItem("ChocMailToken")},
					contentType: "application/json",
					data: JSON.stringify(data),
					success: function() {
						alert("Radi");
					}
				});
				
				
			});

			
			delete_contact.click(function() {

			

				
				$.ajax({
					url: URL + "/" + CONTACT.id,
					type: "DELETE",
					headers: {"Authorization": "Bearer " + localStorage.getItem("ChocMailToken")},
					success: function() {
						alert("Radi");
					}
				});
				
				
				
			});

		}
	});

	
	
	
	
}



function createContact() {

	confirmButton.text("Add contact");
	delete_contact.remove();

	confirmButton.click(function() {

		
		var data= {
				firstName: firstName.val(),
				lastName: lastName.val(),
				displayName: displayName.val(),
				email : email.val(),
				note : note.val()
				
		}

		
		$.ajax({
			url: URL,
			type: "POST",
			headers: {"Authorization": "Bearer " + localStorage.getItem("ChocMailToken")},
			contentType: "application/json",
			data: JSON.stringify(data),
			success: function() {
				alert("Radi");
			}
			
			
		});
	});
}